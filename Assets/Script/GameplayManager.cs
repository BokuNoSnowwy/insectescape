﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;


public enum BadEvent
{
    Wind,
    FallingFeet,
    StunningInsect
}

[System.Serializable]
public class Level
{
    public float timeLevel;
    public int insectGoal;
    public int nbOfInsect;
    public List<BadEvent> listEvent = new List<BadEvent>();
}


public class GameplayManager : MonoBehaviour
{
    [Header("Mouse Detection")] 
    public Vector3 mousePos;
    
    [Header("Level")]
    public List<Level> levelList = new List<Level>();
    public List<InsectBehavior> insectList = new List<InsectBehavior>();
    public InsectArea insectArea;
    public int indexLevel;
    public bool levelIsPlaying;
        
    [Header("UI")]
    public int actualScoreLevel;
    public TextMeshProUGUI scoreLabel;
    
    [Header("TimerLevel")]
    public float remainingTime;
    public bool timeIsDecreasing;
    public TextMeshProUGUI timeLabel;

    [Header("Insects")] 
    public GameObject insectPrefab;
    public List<Sprite> insectSpriteList = new List<Sprite>();

    public static GameplayManager Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else if (Instance != null)
        {
            Destroy(gameObject);
        }
    }
    
    // Start is called before the first frame update
    void Start()
    {
        indexLevel = 0;
        StartGame();
    }

    public void StartGame()
    {
        levelIsPlaying = true;
        RefreshAll();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }
        
        //Get Mouse pos
        //mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y));
        Vector2 mousePose = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x,Input.mousePosition.y));
        mousePos = mousePose;
        
        if (levelIsPlaying)
        {
            if (timeIsDecreasing)
            {
                RefreshTimeLevel();
                
                remainingTime -= Time.deltaTime;
                if (remainingTime <= 0)
                {
                    LevelEnd();
                }
            }
        }
    }

    public void RefreshAll()
    {
        SetupTimeLevel();
        RefreshScore();
        RefreshInsects();
    }

    public void SetupTimeLevel()
    { 
        remainingTime = levelList[indexLevel].timeLevel;
        timeIsDecreasing = true;
    }

    public void RefreshTimeLevel()
    {
        timeLabel.text = Mathf.FloorToInt(remainingTime).ToString();
    }

    public void RefreshScore()
    {
        actualScoreLevel = GetAliveInsectsInArea();
        scoreLabel.text = actualScoreLevel + " / " + levelList[indexLevel].insectGoal;
    }

    public void RefreshInsects()
    {
        foreach (var insect in insectList)
        {
            Destroy(insect.gameObject);
        }
        insectList.Clear();

        for (int i = 0; i < levelList[indexLevel].nbOfInsect; i++)
        {
            float spawnY = Random.Range
                (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).y, Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height)).y);
            float spawnX = Random.Range
                (Camera.main.ScreenToWorldPoint(new Vector2(0, 0)).x, Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0)).x);
 
            Vector3 spawnPosition = new Vector3(spawnX, spawnY,-10f);
                
            //Instanciate un peu partout sur une zone
            GameObject insectToCreate = Instantiate(insectPrefab, spawnPosition, quaternion.identity);
            switch (Random.Range(0,3))
            {
                case 0 :
                    insectToCreate.GetComponent<Animator>().SetBool("Scarabe",true);
                    break;
                case 1 :
                    insectToCreate.GetComponent<Animator>().SetBool("Fourmi",true);
                    break;
                case 2 :
                    insectToCreate.GetComponent<Animator>().SetBool("Gendarme",true);
                    break;
                case 3 :
                    insectToCreate.GetComponent<Animator>().SetBool("Mante",true);
                    break;
            }
            insectList.Add(insectToCreate.GetComponent<InsectBehavior>());
        }
    }

    public void LevelEnd()
    {
        levelIsPlaying = false;
        if (Win())
        {
            Debug.Log("Win");
        }
        else
        {
            Debug.Log("False");
        }
        NextLevel();
    }

    public bool Win()
    {
        return GetAliveInsectsInArea() >= levelList[indexLevel].insectGoal;
    }

    public int GetAliveInsectsInArea()
    {
        int returnNumber = 0;
        foreach (var insect in insectList)
        {
            if (insect.inArea && !insect.isDead)
            {
                returnNumber++;
            }
        }
        return returnNumber;
    }

    public void NextLevel()
    {
        indexLevel++;
        levelIsPlaying = true;
        RefreshAll();
    }
}
