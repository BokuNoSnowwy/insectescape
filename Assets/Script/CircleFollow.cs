﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleFollow : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 pos2 = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
        Vector2 pos2Cam = Camera.main.ScreenToWorldPoint(pos2);
        transform.position = new Vector3(pos2Cam.x,pos2Cam.y,0);
    }
}
