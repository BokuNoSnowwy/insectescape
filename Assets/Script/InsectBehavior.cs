﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class InsectBehavior : MonoBehaviour
{
    public bool inArea;
    public bool isDead;

    public bool isRunningFromCursor;
    

    public float nearMultiplier = 5f;
    public float distanceMouse;


    public bool isPushedByWind;
    
    [Header("Random Movements")] 
    public float timerBeforeChangingDirection;
    public float timerBeforeChangingDirectionMax;

    //public Vector2 randDir;
    //public Quaternion randRot;

    public float moveInsect;
    public float rotInsect;


// Start is called before the first frame update
    void Start()
    {
        timerBeforeChangingDirection = 0;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (!isDead)
        {
            Vector2 pos = new Vector2(transform.position.x, transform.position.y);
            Vector2 mousePos = new Vector2(GameplayManager.Instance.mousePos.x, GameplayManager.Instance.mousePos.y);
        
            //Récupère la distance de l'object par rapport a la souris
            distanceMouse = Vector2.Distance(GameplayManager.Instance.mousePos, transform.position);
            //Check si l'objet est dans la zone d'effet de la souris
            if (Vector2.Distance(GameplayManager.Instance.mousePos, transform.position) < 1)
            {
                isRunningFromCursor = true;
                transform.Translate((pos - mousePos )*nearMultiplier * Time.deltaTime);
                //transform.position = transform.position + (-_gameplayManager.mousePos - transform.position)*distanceMouse*10 * Time.deltaTime;
            }
            else
            {
                isRunningFromCursor = false;
            }
        
            if (!isRunningFromCursor)
            {
                if (!isPushedByWind)
                {
                    timerBeforeChangingDirection -= Time.deltaTime;
                    if (timerBeforeChangingDirection <= 0)
                    {
                        //randDir = new Vector2(Random.Range(-moveInsect, moveInsect), Random.Range(-moveInsect, moveInsect))* Time.deltaTime;
                        //randRot = new Quaternion(Random.Range(-rotInsect, rotInsect),Random.Range(-rotInsect, rotInsect),0f,0f);
                        transform.rotation = Quaternion.Euler(0f,0f,Random.Range(-rotInsect, rotInsect));
                        timerBeforeChangingDirection = Random.Range(timerBeforeChangingDirectionMax-0.25f,timerBeforeChangingDirectionMax+0.25f);
                    }
                    else
                    {
                        //transform.Translate(randDir);
                        //transform.Translate(transform.up*Time.deltaTime);
                        transform.position += transform.up*0.5f * Time.deltaTime;
                    }
                }
            }
            else
            {
                transform.rotation = Quaternion.Euler(0f,0f,0f);
                // vec = pos - mousePos;
            }
        }
        else
        {
            GetComponent<SpriteRenderer>().DOColor(Color.red, 0.1f);
            GetComponent<Animator>().enabled = false;
        }
    }

    public void Die()
    {
        //Faire la giclure de sang
        isDead = true;
    }
}
