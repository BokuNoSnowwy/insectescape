﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Unity.Mathematics;
using UnityEngine;
using Random = UnityEngine.Random;

public class InsectArea : MonoBehaviour
{

    public float timerEvent;
    private float timerEventMax;

    public float aleaEventOffset;

    [Header("FallingFeet")]
    public GameObject feetShadow;
    public GameObject prefabFeet;

    [Header("Wind")]
    public GameObject windPrefab;
    public Transform windSpot;
    // Start is called before the first frame update
    void Start()
    {
        timerEventMax = timerEvent;
    }

    // Update is called once per frame
    void Update()
    {
        if (timerEvent > 0)
        {
            timerEvent -= Time.deltaTime;
        }
        else
        {
            TriggerEvent();
            timerEvent = Random.Range(timerEventMax-aleaEventOffset,timerEventMax+aleaEventOffset);
        }
    }

    void TriggerEvent()
    {
        Level actualLevel = GameplayManager.Instance.levelList[GameplayManager.Instance.indexLevel];
        BadEvent badEvent = actualLevel.listEvent[Random.Range(0, actualLevel.listEvent.Count - 1)];


        switch (badEvent)
        {
            case BadEvent.Wind :
                Debug.Log("Lance Alea : " + badEvent);
                WindEvent();
                break;
                
            case BadEvent.FallingFeet :
                Debug.Log("Lance Alea : " + badEvent);
                FallingFeetEvent();
                break;
            
            case BadEvent.StunningInsect :
                Debug.Log("Lance Alea : " + badEvent);
                break;
        }
    }

    void FallingFeetEvent()
    {
        List<GameObject> aliveInsectList =new List<GameObject>();
        foreach (var insect in GameplayManager.Instance.insectList)
        {
            if (!insect.isDead)
            {
                aliveInsectList.Add(insect.gameObject);
            }
        }

        GameObject randomInsect = aliveInsectList[Random.Range(0, aliveInsectList.Count - 1)];
        GameObject shadowFeet = Instantiate(feetShadow, randomInsect.transform.position, quaternion.identity);
        shadowFeet.transform.localScale = new Vector3(0.25f,0.25f,0.25f);
        shadowFeet.transform.DOScale(new Vector3(0.75f, 0.75f, 0.75f), 2.5f).OnComplete(() =>
            InstanciateFeet(shadowFeet));
    }

    void InstanciateFeet(GameObject shadowFeet)
    {
        Debug.Log("InstanciateFeet");
        Instantiate(prefabFeet, shadowFeet.transform.position, quaternion.identity);
        Destroy(shadowFeet);
    }

    void WindEvent()
    {
        GameObject wind = Instantiate(windPrefab, windSpot.position, quaternion.identity);
        wind.transform.rotation = Quaternion.Euler(0f,0f,Random.Range(0f,360f));
    }
    

    private void OnTriggerExit2D(Collider2D other)
    {
        CheckInsect(other, false);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        CheckInsect(other, true);
    }

    public void CheckInsect(Collider2D other, bool inArea)
    {
        if (other.gameObject.GetComponent<InsectBehavior>())
        {
            InsectBehavior insect = other.GetComponent<InsectBehavior>();
            insect.inArea = inArea;
            GameplayManager.Instance.RefreshScore();
        }
    }
}
