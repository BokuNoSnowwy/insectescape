﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildFeetEvent : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Destroy",2f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<InsectBehavior>())
        {
            InsectBehavior insect = other.GetComponent<InsectBehavior>();
            insect.Die();
            GameplayManager.Instance.RefreshScore();
        }
    }

    private void Destroy()
    {
        Destroy(gameObject);
    }
}
