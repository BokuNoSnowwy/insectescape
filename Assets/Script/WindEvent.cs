﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindEvent : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Destroy",4f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<InsectBehavior>())
        {
            InsectBehavior insect = other.GetComponent<InsectBehavior>();
            insect.isPushedByWind = true;
            //Debug.Log("Pushing Insect");
            //insect.transform.Translate(-transform.right*Time.deltaTime);
            insect.transform.position += -transform.right * Time.deltaTime;
            GameplayManager.Instance.RefreshScore();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<InsectBehavior>())
        {
            InsectBehavior insect = other.GetComponent<InsectBehavior>();
            insect.isPushedByWind = false;
        }
    }

    private void Destroy()
    {
        Destroy(gameObject);
    }
}
